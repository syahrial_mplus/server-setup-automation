#!/usr/bin/env bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" << EOF
    CREATE USER syahrial;
    ALTER ROLE syahrial WITH PASSWORD '123qwerty';
    ALTER USER syahrial CREATEDB;
EOF