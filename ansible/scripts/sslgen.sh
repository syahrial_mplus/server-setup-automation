#!/usr/bin/env bash

set -ex
mkdir -p /tmp/sslgen
cd /tmp/sslgen
hash snap || apt install snapd -y
hash openssl || apt install openssl -y
cat << EOF > certbot.ini
dns_cloudflare_api_token = $1
EOF
chmod 700 certbot.ini

if [[ ! -d "/snap/certbot" ]]; then
    snap install core
    snap refresh core
    snap install --classic certbot
fi

if [[ ! -d "/snap/certbot-dns-cloudflare" ]]; then
    snap set certbot trust-plugin-with-root=ok
    snap install certbot-dns-cloudflare
fi

certbot certonly --dns-cloudflare \
    --dns-cloudflare-credentials certbot.ini \
    --non-interactive --agree-tos \
    -d "$2" -d "*.$2" \
    --email "$3" 

cd /
rm -rf /tmp/sslgen

if [[ ! -e "/etc/letsencrypt/dhparam.pem" ]]; then
    openssl dhparam -out /etc/letsencrypt/dhparam.pem 1024 &> /tmp/dhparam.out
    rm /tmp/dhparam.out
fi

chown -R 101:101 /etc/letsencrypt