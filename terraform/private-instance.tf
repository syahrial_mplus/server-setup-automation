resource "aws_instance" "private" {
    count                       = length(var.private_instances)
    ami                         = "ami-09e67e426f25ce0d7"
    instance_type               = var.private_instances[count.index].type
    associate_public_ip_address = false
    key_name                    = aws_key_pair.demo_key.key_name
    private_ip                  = var.private_instances[count.index].ip
    security_groups             = [ aws_security_group.private_sg.id ]
    subnet_id                   = aws_subnet.vpc_a_sub_private.id
    tags = {
        Name = var.private_instances[count.index].name
    }
}