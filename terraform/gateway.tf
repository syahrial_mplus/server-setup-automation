resource "aws_eip_association" "gateway_eip" {
    instance_id     = aws_instance.gateway.id
    allocation_id   = var.eip_id
}

resource "aws_instance" "gateway" {
    ami                         = "ami-09e67e426f25ce0d7"
    instance_type               = "t2.micro"
    associate_public_ip_address = false
    key_name                    = aws_key_pair.demo_key.key_name
    private_ip                  = var.public_instance.ip
    security_groups             = [ aws_security_group.public_sg.id ]
    subnet_id                   = aws_subnet.vpc_a_sub_public.id
    source_dest_check           = false
    tags = {
        Name = var.public_instance.name
    }
}

resource "aws_route_table" "private_route" {
    vpc_id  = aws_vpc.vpc_a.id
    route {
        cidr_block  = "0.0.0.0/0"
        instance_id = aws_instance.gateway.id
    }
}

resource "aws_route_table_association" "private_route_assoc" {
    subnet_id      = aws_subnet.vpc_a_sub_private.id
    route_table_id = aws_route_table.private_route.id
}