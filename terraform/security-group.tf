# Create a security group that allows port 22, 80, and 443 
# from the outside world

resource "aws_security_group" "public_sg" {
    name        = "sg"
    description = "Allow gateway to be connected to the outside world"
    vpc_id      = aws_vpc.vpc_a.id
    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }
    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }
    ingress {
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }
    ingress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = [ aws_vpc.vpc_a.cidr_block ]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    tags = {
        Name = "public_sg"
    }
}


resource "aws_security_group" "private_sg" {
    name        = "private_sg"
    description = "General security group for private instances"
    vpc_id      = aws_vpc.vpc_a.id
    ingress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = [ "0.0.0.0/0" ]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags = {
        Name = "private_sg"
    }
}
