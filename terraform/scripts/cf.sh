#!/usr/bin/env bash

CF_EMAIL="${CF_EMAIL:-lemniskett@outlook.com}"
CF_API_KEY="${CF_API_KEY:-l1H8CoeUXeAsXWu7PgeVTZBRqXoLeeKBocvXFqNb}"
CF_ZONE_KEY="${CF_ZONE_KEY:-bd03a208cdfc5a9aabcdfbc848fcf07d}"

case $1 in
    create)
    curl -X POST "https://api.cloudflare.com/client/v4/zones/${CF_ZONE_KEY}/dns_records" \
        -H "X-Auth-Email: ${CF_EMAIL}" \
        -H "Authorization: Bearer ${CF_API_KEY}" \
        -H "Content-Type: application/json" \
        --data '{"type":"A","name":"'${2}'","content":"'${3}'","ttl":120,"priority":10,"proxied":false}' | jq
    ;;
    update)
    curl -X PUT "https://api.cloudflare.com/client/v4/zones/${CF_ZONE_KEY}/dns_records/${3}" \
        -H "X-Auth-Email: ${CF_EMAIL}" \
        -H "Authorization: Bearer ${CF_API_KEY}" \
        -H "Content-Type: application/json" \
        --data '{"type":"A","name":"'${2}'","content":"'${4}'","ttl":120,"priority":10,"proxied":false}' | jq
    ;;
    *)
    echo "What?" 1>&2
    exit 1
    ;;
esac
