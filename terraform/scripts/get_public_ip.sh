#!/usr/bin/env bash

script_dir="$(cd "$(dirname "$0")" && pwd)"
tfstate_file="${script_dir}/../terraform.tfstate"

command -v jq &>/dev/null \
    || { echo "jq is required for this script to work"; exit 1; }

jq -r '.resources[]
    | select(.type == "aws_eip")
    | .instances[].attributes.public_ip' < "$tfstate_file" 