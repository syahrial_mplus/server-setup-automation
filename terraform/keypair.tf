# Create a key pair for the instance, which is generated using
# `keygen.sh` in the `keygen` directory.

resource "aws_key_pair" "demo_key" {
    key_name   = "demo_key"
    public_key = file(var.keypair.path)
}