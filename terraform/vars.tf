variable "cloud_provider" {
  type = object({
    region  = string
    profile = string
  })

  default = {
    profile = "default"
    region  = "us-east-1"
  }
}

variable "intranet" {
  type = object({
    cidr            = string
    public_cidr     = string
    private_cidr    = string
  })
  default = {
    cidr            = "192.168.200.0/24"
    public_cidr     = "192.168.200.0/25"
    private_cidr    = "192.168.200.128/25"
  }
}

variable "public_instance" {
  type = object({
    name    = string
    ip      = string
  })

  default = {
    ip      = "192.168.200.4"
    name    = "gateway"
  }
}

variable "private_instances" {
  type = list(object({
    name    = string
    ip      = string
    type    = string
  }))
  default = [
    {
      name    = "odoo"
      ip      = "192.168.200.132"
      type    = "t2.small"
    },
    {
      name    = "db"
      ip      = "192.168.200.133"
      type    = "t2.micro"
    }
  ]
}

variable "keypair" {
  type = object({
    path    = string
  })

  default = {
    path    = "../keys/mplus_demo.pem.pub"
  }
}

variable "eip_id" {
  type = string
  default = "eipalloc-0f57da037a18e4084"
}