# Create a VPC and other necessary components

resource "aws_vpc" "vpc_a" {
    cidr_block        = var.intranet.cidr
    instance_tenancy  = "default"
    tags = {
        Name = "vpc_a"
    }
}

resource "aws_internet_gateway" "vpc_a_ig" {
    vpc_id  = aws_vpc.vpc_a.id
    tags = {
        Name = "vpc_a_ig"
    }
}

resource "aws_subnet" "vpc_a_sub_public" {
    vpc_id = aws_vpc.vpc_a.id
    cidr_block = var.intranet.public_cidr
    tags = {
        Name = "vpc_a_sub_public"
    }
}

resource "aws_subnet" "vpc_a_sub_private" {
    vpc_id = aws_vpc.vpc_a.id
    cidr_block = var.intranet.private_cidr
    tags = {
        Name = "vpc_a_sub_private"
    }
}

resource "aws_default_route_table" "vpc_a_def_route" {
    default_route_table_id  = aws_vpc.vpc_a.default_route_table_id
    route {
        cidr_block  = "0.0.0.0/0"
        gateway_id  = aws_internet_gateway.vpc_a_ig.id
    }
}