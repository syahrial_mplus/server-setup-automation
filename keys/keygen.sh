#!/usr/bin/env bash

dir="$(dirname "$0")"
ssh-keygen -N "" -f "${dir}"/mplus_demo.pem -C "mplus"

chmod -R 700 "${dir}"